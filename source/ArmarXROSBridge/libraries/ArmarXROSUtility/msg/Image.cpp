/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSSubscriberCallback
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <cv_bridge/cv_bridge.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/OpenCVUtil.h>

#include "Image.h"

namespace armarx
{

    cv::Mat rosImgToCV(const sensor_msgs::Image::ConstPtr& msg)
    {
        ARMARX_CHECK_NOT_NULL(msg);
        cv::Mat fixedClr;
        {
            const auto cvImage = cv_bridge::toCvShare(msg, msg->encoding);
            const cv::Mat& mat = cvImage->image;
            if (mat.channels() == 1)
            {
                double scale = 1;
                if (mat.type() == CV_32F || mat.type() == CV_64F)
                {
                    //float image scale [0, 1] -> [0, 255]
                    scale = 255;
                }
                ARMARX_TRACE;
                fixedClr = cv::Mat{mat.size(), CV_8UC3};
                cv::Mat tmp;
                cvImage->image.convertTo(tmp, CV_8U, scale);
                cv::Mat in[] = {tmp, tmp, tmp};
                cv::merge(in, 3, fixedClr);
            }
            else
            {
                ARMARX_TRACE;
                fixedClr = cv_bridge::toCvShare(msg, "rgb8")->image;
            }
        }
        ARMARX_CHECK_EQUAL(3, fixedClr.channels());
        return fixedClr;
    }
    void rosImgToCByteImage(const sensor_msgs::Image::ConstPtr& msg, CByteImage* img)
    {
        ARMARX_CHECK_NOT_NULL(msg);
        ARMARX_CHECK_NOT_NULL(img);
        cv::Mat fixedClr = rosImgToCV(msg);
        ARMARX_CHECK_EQUAL(3, fixedClr.channels());
        ARMARX_CHECK_EQUAL(img->bytesPerPixel, static_cast<int>(fixedClr.elemSize()));

        if (
            msg->height != static_cast<std::uint32_t>(img->height) ||
            msg->width  != static_cast<std::uint32_t>(img->width)
        )
        {
            ARMARX_TRACE;
            // scale opencv image
            cv::Size size(img->width, img->height);
            cv::Mat dst;
            cv::resize(fixedClr, dst, size); //resize image
            visionx::copyCvMatToIVT(dst, img);
        }
        else
        {
            visionx::copyCvMatToIVT(fixedClr, img);
        }
    }

    void rosDepthImgToCByteImage(const sensor_msgs::Image::ConstPtr& msg, CByteImage* img)
    {
        const auto cvImage = cv_bridge::toCvShare(msg, msg->encoding);
        if (
            msg->height != static_cast<std::uint32_t>(img->height) ||
            msg->width  != static_cast<std::uint32_t>(img->width)
        )
        {
            ARMARX_TRACE;
            // scale opencv image
            cv::Size size(img->width, img->height);
            cv::Mat dst;
            cv::resize(cvImage->image, dst, size); //resize image
            visionx::convertDepthInMetersToWeirdArmarX(dst, img);
        }
        else
        {
            visionx::convertDepthInMetersToWeirdArmarX(cvImage->image, img);
        }

    }
}

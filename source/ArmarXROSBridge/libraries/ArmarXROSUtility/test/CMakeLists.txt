
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ArmarXROSUtility)
 
armarx_add_test(ArmarXROSUtilityTest ArmarXROSUtilityTest.cpp "${LIBS}")
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSSubscriberCallback
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/util/OnScopeExit.h>

#include "ROSManager.h"

namespace armarx
{
    void initROS()
    {
        if (!ros::isInitialized())
        {
            static std::vector<std::string> cliParams;
            static std::vector<char*> argv;
            const auto app = Application::getInstance();
            ARMARX_CHECK_NOT_NULL(app);
            cliParams = app->getCommandLineArguments();
            argv.reserve(cliParams.size() + 1);
            for (auto& arg : cliParams)
            {
                argv.emplace_back(arg.data());
            }
            argv.emplace_back(nullptr);
            auto argc = static_cast<int>(cliParams.size());
            ARMARX_INFO << "initializing ROS with this node name: " << app->getName();
            ARMARX_INFO << "initializing ROS with these arguments: " << cliParams;

            ros::init(argc, argv.data(), app->getName(), ros::init_options::NoSigintHandler);
            static ARMARX_ON_SCOPE_EXIT
            {
                std::cout << "Shutting down ROS" << std::endl;
                ros::shutdown();
            };
        }
    }

    ROSManager::ROSManager(ManagedIceObject& owner) :
        _owner{&owner}
    {
        //initialize ros
        initROS();
    }

    void ROSManager::initialize(const std::string& ns, const ros::M_string& remappings)
    {
        ARMARX_CHECK(_nh == nullptr);
        _nh = std::make_unique<ros::NodeHandle>(ns, remappings);
    }

    void ROSManager::startROSLoop(double rate, std::function<void()> fn)
    {
        ARMARX_CHECK_NOT_NULL(_nh);
        ARMARX_CHECK(!_thread.joinable());
        _stopThread = false;

        _thread = std::thread
        {
            [rate, fn = std::move(fn), this]
            {
                ARMARX_INFO << "wait for started state";
                _owner->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);

                ros::Rate loopRate(rate);
                ARMARX_INFO << "entering ROS loop";
                ARMARX_ON_SCOPE_EXIT{ARMARX_INFO << "leaving ROS loop";};
                while (ros::ok() && !_stopThread)
                {
                    ARMARX_DEBUG << "ros::spinOnce()...";
                    spinOnce();
                    fn();
                    ARMARX_DEBUG << "sleep";
                    loopRate.sleep();
                }
            }
        }
    }
    void ROSManager::stopROSLoop()
    {
        ARMARX_CHECK(_thread.joinable());
        _stopThread = true;
        _thread.join();
    }
    void ROSManager::spinOnce()
    {
        ros::spinOnce();
    }
}

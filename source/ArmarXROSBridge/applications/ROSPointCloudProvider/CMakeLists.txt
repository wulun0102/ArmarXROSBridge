armarx_component_set_name("ROSPointCloudProviderApp")

armarx_build_if(catkin_FOUND "catkin not available")
armarx_build_if(VisionX_FOUND "VisionX not available")

set(COMPONENT_LIBS ROSPointCloudProvider)
armarx_add_component_executable(main.cpp)

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXROSBridge::ArmarXObjects::ROSImageProvider
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <VisionX/tools/ImageUtil.h>

#include <ArmarXROSBridge/libraries/ArmarXROSUtility/msg/Image.h>

#include "ROSImageProvider.h"

namespace armarx
{
    void ROSImageProvider::onInitImageProvider()
    {
        _ros.initialize();
        const auto rosImageTopics = armarx::Split(getProperty<std::string>("ROSTopicImages"), ",;", true, true);
        const auto rosDepthImageTopics = armarx::Split(getProperty<std::string>("ROSTopicImageDepth"), ",;", true, true);
        ARMARX_INFO << "Using ROS Topics "
                    << "\nimages: " << rosImageTopics
                    << "\ndepth images:" << rosDepthImageTopics;
        //info about provided images
        visionx::ImageFormatInfo imageFormat;
        {
            const Eigen::Vector2i dim = getProperty<Eigen::Vector2i>("Dimensions");
            imageFormat.dimension.width  = dim(0);
            imageFormat.dimension.height = dim(1);
            imageFormat.bytesPerPixel = 3;
            imageFormat.type = visionx::ImageType::eRgb;
        }

        const int nimages = static_cast<int>(rosImageTopics.size() + rosDepthImageTopics.size());
        setNumberImages(nimages);
        setImageFormat(imageFormat.dimension, imageFormat.type);


        //subscribe to image topics
        std::vector<CByteImage*> resultImagePtrs;
        {
            for (const auto& name : rosImageTopics)
            {
                std::shared_ptr<CByteImage> img{visionx::tools::createByteImage(imageFormat, imageFormat.type)};
                resultImagePtrs.emplace_back(img.get());
                _ros.subscribe
                (
                    name,
                    1,
                    boost::function<void(const sensor_msgs::Image::ConstPtr&)>
                {
                    [this, name, img](const sensor_msgs::Image::ConstPtr & msg)
                    {
                        ARMARX_INFO << deactivateSpam(10, name)
                                    << "[ " << name << "] got image "
                                    << msg->width << "x" << msg->height;

                        rosImgToCByteImage(msg, img.get());
                        const auto t = static_cast<Ice::Long>(msg->header.stamp.toNSec() / 1000);
                        _imageMinTimestampMs = std::min(_imageMinTimestampMs, t);
                    }
                });
            }
            for (const auto& name : rosDepthImageTopics)
            {
                std::shared_ptr<CByteImage> img{visionx::tools::createByteImage(imageFormat, imageFormat.type)};
                resultImagePtrs.emplace_back(img.get());
                _ros.subscribe
                (
                    name,
                    1,
                    boost::function<void(const sensor_msgs::Image::ConstPtr&)>
                {
                    [this, name, img](const sensor_msgs::Image::ConstPtr & msg)
                    {
                        ARMARX_INFO << deactivateSpam(10, name)
                                    << "[ " << name << "] got depth image "
                                    << msg->width << "x" << msg->height;

                        rosDepthImgToCByteImage(msg, img.get());
                        const auto t = static_cast<Ice::Long>(msg->header.stamp.toNSec() / 1000);
                        _imageMinTimestampMs = std::min(_imageMinTimestampMs, t);
                    }
                });
            }
        }

        _imageMinTimestampMs = std::numeric_limits<Ice::Long>::max();
        _ros.startROSLoop
        (
            getProperty<double>("MaxFPS"),
            [this, resultImagePtrs]()mutable
        {
            if (_imageMinTimestampMs != std::numeric_limits<Ice::Long>::max())
            {
                updateTimestamp(_imageMinTimestampMs);
                provideImages(resultImagePtrs.data());
            }
            _imageMinTimestampMs = std::numeric_limits<Ice::Long>::max();
        }
        );
    }

    void ROSImageProvider::onExitImageProvider()
    {
        ARMARX_INFO << "stopping onExitImageProvider";
        _ros.stopROSLoop();
    }

    PropertyDefinitionsPtr ROSImageProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ROSImageProviderPropertyDefinitions(
                getConfigIdentifier()));
    }
}


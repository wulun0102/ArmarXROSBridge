armarx_component_set_name("ROSImageProvider")

armarx_build_if(catkin_FOUND "catkin not available")
armarx_build_if(VisionX_FOUND "VisionX not available")

set(COMPONENT_LIBS ArmarXROSUtility)
set(SOURCES ROSImageProvider.cpp)
set(HEADERS ROSImageProvider.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

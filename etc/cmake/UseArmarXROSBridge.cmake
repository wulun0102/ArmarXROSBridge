#  This file contains macros for projects depending on ArmarXROSBridge





macro(collect_interface_dirs project)
    if(NOT ${project}_FOUND)
        find_package(${project} REQUIRED)
    endif()
    list(APPEND collected_interface_dirs ${${project}_INTERFACE_DIRS})
    message(STATUS "${project} collected_interface_dirs ${collected_interface_dirs}")
    message(STATUS "${project}_INTERFACE_DEPENDENCIES ${${project}_INTERFACE_DEPENDENCIES}")
    foreach(p ${${project}_INTERFACE_DEPENDENCIES})
        collect_interface_dirs(${p})
    endforeach()
endmacro()

function(preprocess_slice_file project slice_file_rel output_file)
    collect_interface_dirs(${project})
    list(REMOVE_DUPLICATES collected_interface_dirs)
    set(slice_dir ${${project}_INTERFACE_DIRS})
    set(slice_file ${slice_dir}/${slice_file_rel})
    message(STATUS "slice_file ${slice_file}")
    if(NOT EXISTS "${slice_file}")
        message(FATAL_ERROR "requested to process ${slice_file_rel} from project ${project}. (File does not Exist)")
    endif()
    set(flags "-I${Ice_Slice_DIR}")
    foreach(d ${collected_interface_dirs})
        list(APPEND flags "-I${d}")
    endforeach()
    execute_process(
        COMMAND cpp ${slice_file} ${flags}
#        [COMMAND <cmd2> [args2...] [...]]
#        [WORKING_DIRECTORY <directory>]
#        [TIMEOUT <seconds>]
#        [RESULT_VARIABLE <variable>]
        OUTPUT_VARIABLE preprocessed
#        [ERROR_VARIABLE <variable>]
#        [INPUT_FILE <file>]
#        [OUTPUT_FILE <file>]
#        [ERROR_FILE <file>]
#        [OUTPUT_QUIET]
#        [ERROR_QUIET]
#        [OUTPUT_STRIP_TRAILING_WHITESPACE]
#        [ERROR_STRIP_TRAILING_WHITESPACE]
    )
    file(WRITE "${output_file}" "${preprocessed}")
endfunction()
